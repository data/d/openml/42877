# OpenML dataset: SCADI

https://www.openml.org/d/42877

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: S.M.M. Fatemi Bushehri,Moslem Dehghanizadeh,Shokoofeh Kalantar,Mohsen Sardari Zarchi
**Source**: UCI - 1994
**Please cite**: See BibTeX citation

**SCADI dataset**
This dataset contains 206 attributes of 70 children with physical and motor disability based on ICF-CY. In particular, the SCADI dataset is the only one that has been used by ML researchers for self-care problems classification based on ICF-CY to this date. The 'Class' field refers to the presence of the self-care problems of the children with physical and motor disabilities.The classes are determined by occupational therapists. The names and social security numbers of the children were recently removed from the dataset.

Two files have been 'processed', SCADI.arff for using in WEKA and SCADI.CSV for using in MATLAB and similar tools.

### Attribute information:

1. gender: gender (1 = male; 0 = female)
2. age: age in years 
3-205. self-care activities based on ICF-CY (1 = The case has this feature; 0 = otherwise)
206. Classes ( class1 = Caring for body parts problem; class2 = Toileting problem; class3 = Dressing problem; class4 = Washing oneself and Caring for body parts and Dressing problem; class5 = Washing oneself, Caring for body parts, Toileting, and Dressing problem; class6 = Eating, Drinking, Washing oneself, Caring for body parts, toileting,Dressing, Looking after health and Looking after safety problem; class7 = No Problem; )

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42877) of an [OpenML dataset](https://www.openml.org/d/42877). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42877/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42877/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42877/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

